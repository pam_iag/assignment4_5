#include <stdio.h>
#include <stdlib.h>

int main()
{
    int j,i,n;
    printf("Input number to which the multiplication table should go upto : ");
    scanf("%d",&n);
    for(i=1;i<=10;i++){
        for(j=1;j<=n;j++){
            if (j<=n-1)
                printf("%d x %d = %d    ",j,i,i*j);
            else
                printf("%d x %d = %d    ",j,i,i*j);

        }
    printf("\n");
    }
    return 0;
}
